import { Component, OnInit } from '@angular/core';
import { DonneesService, NoteInfo } from '../donnees.service';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
 title = 'Laboratoire 28';
  notes: NoteInfo[] = [];
  isLoading = false;
  codep: any = "";
  note: any = 0;
  codepermanent = new FormControl('');
  noteetud = new FormControl('');
  savestatus = "";
  errorMessageCodeP ="";
  errorMessageNote = "";
  adminDisplay: boolean = false;
  constructor(private ds:DonneesService) {}


  ngOnInit(): void {
  }
  addNote() {
    let observ=this.ds.getNote()
      .subscribe((data: NoteInfo[]) => this.notes = data);

    // arrete l'ecoute après 100 ms seconds
    setTimeout(() => {
      observ.unsubscribe();// arreter l'ecoute
      this.adminDisplay = true;
      let newelem = { 'codepermanent': this.codepermanent.value, 'note': this.noteetud.value };
      this.errorMessageCodeP = "";
      this.errorMessageNote = "";
  if (this.validateCodePermanent(this.codepermanent.value.trim()) && this.validateNote(this.noteetud.value.trim())) {
    let update = false;
    this.notes.forEach(elem => {
      if (elem.codepermanent.trim() == this.codepermanent.value.trim()) {
          //si le code permetant existe deja dans le tableau, donc maj
          elem.note = this.noteetud.value;
          update = true;
        }
    });
    if(!update){//si pas maj, ajouter element dans le tableau
       this.notes.push(newelem)
    }

      console.log(newelem);
      console.log(this.notes);
  } else {
    console.log("Erreur: données invalides");
    console.log(newelem);
    }
}, 100);

  }

  saveNotes() {// enregistrer les donnees en appelant la methode http post
    let datatosave = this.notes;
    console.log('savenote');
    console.log(datatosave);
    let filename = 'notes2.json';

    let obs = this.ds.save(datatosave, filename)
      .subscribe(
        (data: any) => this.savestatus = data
    );
    setTimeout(() => {
      console.log(this.savestatus);
      this.addNote();
    },1000);
  }

  private validateCodePermanent(codep: string): boolean{
    let regexp:RegExp = new RegExp("^[A-Z]{4}[0-9]{8}$");
    let valcodepstatus=regexp.test(codep);
    if(!valcodepstatus) {
      this.errorMessageCodeP= "Codepermanent invalide: " + codep + " ";
      this.errorMessageCodeP+=" --> 4 lettres Majuscules suivies de 8 chiffres svp!"
    }
    return (valcodepstatus);
  }

  private validateNote(note:string):boolean{
    let regexp: RegExp = new RegExp("^(([0-9]){1}|([1-9][0-9]){2}|(100))");
    let valnotestatus=regexp.test(note);
    if (!valnotestatus) {
      this.errorMessageNote = "Note invalide: " + note + " ";
      this.errorMessageNote += " --> Nombre entier entre 0 et 100 compris";
    }
    return(valnotestatus);
  }
}
