import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';
import { Router } from '@angular/router';
// certains code proviennent de https://www.positronx.io/angular-jwt-user-authentication-tutorial/
@Injectable({
  providedIn: 'root'
})
export class DonneesService {
  private notes: NoteInfo[] = [];
  private endpoint: string = 'http://localhost:4000/';
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  currentUser = {};
  constructor(private http: HttpClient, public router: Router) { }

  /*
  * appelle la method http.get pour prendre les notes
  */
  getNote() {
    let dataurl: string = "http://localhost:3000/getjson?f=notes2.json";
    let reqoption = { observe: 'body', responseType: 'json' };

    return this.http.get<NoteInfo[]>(dataurl);

  }

/*
* appelle la méthode http.post pour enregistrer les notes dans le fichier spécifier
* ATTENTION: params est passés come body de la requettes
* les clés utilisées 'data' et 'file' sont celle reconues
* par le endpoint utilisé -- application tpapp
* datatosave est un json contenant les notes (voir interface NoteInfo ci-après)
* filename contient le nom du fichier qui va contenir les données (datatosave) sur le serveur (backend).
*/
  save(datatosave: any,filename:string) {
    let posturl: string = "http://localhost:3000/postjson";
    const params = {
      'data': JSON.stringify(datatosave),
      'file': filename
    };
    console.log("datatosave");
    console.log(datatosave);
    console.log(params);
     return this.http.post<any>(posturl,params);
  }

}

export interface NoteInfo{
  codepermanent: string;
  note: number;
}

export interface User {
  id: String;
  username: String;
  firstName: String;
  lastName: String;
  token: String;
  password: String;
}
